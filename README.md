# Chạy project bằng ở máy cá nhân

1. Clone project https://gitlab.com/cnpmm-thaythai/cnpmm-be.git
2. Di chuyển đến thư mục cnpmm-be
3. Chạy lệnh dotnet tool install --global dotnet-ef để cài dotnet ef
4. Thay đổi các thông tin về username, database, password ở file appsettings.json, và tạo database mysql
5. Chạy lệnh dotnet ef database update
6. Chạy lệnh dotnet run để chạy project.

# Chạy project bằng docker

Truy cập đến https://gitlab.com/cnpmm-thaythai/cnpmm-deployment và làm theo các bước hướng dẫn.
