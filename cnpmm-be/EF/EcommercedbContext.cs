﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace cnpmm_be.Entities;

public partial class EcommercedbContext : DbContext
{
    public EcommercedbContext()
    {
    }

    public EcommercedbContext(DbContextOptions<EcommercedbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<BrandEntity> BrandEntities { get; set; }

    public virtual DbSet<CategoryEntity> CategoryEntities { get; set; }

    public virtual DbSet<OrderEntity> OrderEntities { get; set; }

    public virtual DbSet<OrderItemEntity> OrderItemEntities { get; set; }

    public virtual DbSet<ProductAttributeEntity> ProductAttributeEntities { get; set; }

    public virtual DbSet<ProductEntity> ProductEntities { get; set; }

    public virtual DbSet<ProductSpecEntity> ProductSpecEntities { get; set; }

    public virtual DbSet<UserEntity> UserEntities { get; set; }

    public virtual DbSet<ProductImageEntity> ProductImageEntities {get; set;}

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_0900_ai_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<BrandEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("brand_entity");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.Image)
                .HasMaxLength(255)
                .HasColumnName("image");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name");
        });

        modelBuilder.Entity<CategoryEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("category_entity");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.Image)
                .HasMaxLength(255)
                .HasColumnName("image");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name");
            entity.Property(e => e.Description)
                .HasMaxLength(255)
                .HasColumnName("description");
        });

        modelBuilder.Entity<OrderEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("order_entity");

            entity.HasIndex(e => e.UserId, "FK4na8ykxyemvs9auhondwoj7ir");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.Status)
                .HasMaxLength(255)
                .HasColumnName("status");
            entity.Property(e => e.ReceiverPhone)
                .HasMaxLength(255)
                .HasColumnName("receiver_phone");
            entity.Property(e => e.ReceiverName)
                .HasMaxLength(255)
                .HasColumnName("receiver_name");
            entity.Property(e => e.Address)
                .HasMaxLength(255)
                .HasColumnName("address");
            entity.Property(e => e.Payment)
                .HasMaxLength(255)
                .HasColumnName("payment");
            entity.Property(e => e.IsPaid)
                .HasColumnType("bit(1)")
                .HasColumnName("is_paid");
            entity.Property(e => e.Total).HasColumnName("total");
            entity.Property(e => e.UserId).HasColumnName("user_id");

            entity.HasOne(d => d.User).WithMany(p => p.OrderEntities)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK4na8ykxyemvs9auhondwoj7ir");
        });

        modelBuilder.Entity<OrderItemEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("order_item_entity");

            entity.HasIndex(e => e.ProductId, "FKe17bu5pl07v13gm6pxjw8l87e");

            entity.HasIndex(e => e.OrderId, "FKfok8dv4hj3mw9wixok1qq15xr");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.OrderId).HasColumnName("order_id");
            entity.Property(e => e.ProductId).HasColumnName("product_id");
            entity.Property(e => e.ProductName)
                .HasMaxLength(255)
                .HasColumnName("product_name");
            entity.Property(e => e.ProductPrice).HasColumnName("product_price");
            entity.Property(e => e.Quantity).HasColumnName("quantity");

            entity.HasOne(d => d.Order).WithMany(p => p.OrderItemEntities)
                .HasForeignKey(d => d.OrderId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FKfok8dv4hj3mw9wixok1qq15xr");

            entity.HasOne(d => d.Product).WithMany(p => p.OrderItemEntities)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FKe17bu5pl07v13gm6pxjw8l87e");
        });

        modelBuilder.Entity<ProductAttributeEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("product_attribute_entity");

            entity.HasIndex(e => e.CategoryId, "FKf7u5lln5u3rggcmajqtay4ug4");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CategoryId).HasColumnName("category_id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name");

            entity.HasOne(d => d.Category).WithMany(p => p.ProductAttributeEntities)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FKf7u5lln5u3rggcmajqtay4ug4");
        });

        modelBuilder.Entity<ProductEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("product_entity");

            entity.HasIndex(e => e.CategoryId, "FK8kxxmqdokh3lthvw0t148w0bc");

            entity.HasIndex(e => e.BrandId, "FKj94dnak6ecn8sf4oc862mndw3");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.BrandId).HasColumnName("brand_id");
            entity.Property(e => e.CategoryId).HasColumnName("category_id");
            entity.Property(e => e.Color)
                .HasMaxLength(255)
                .HasColumnName("color");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.Description)
                .HasColumnType("text")
                .HasColumnName("description");
            entity.Property(e => e.Guarantee)
                .HasMaxLength(255)
                .HasColumnName("guarantee");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name");
            entity.Property(e => e.OnSale)
                .HasColumnType("bit(1)")
                .HasColumnName("on_sale");
            entity.Property(e => e.Price).HasColumnName("price");
            entity.Property(e => e.Quantity).HasColumnName("quantity");
            entity.Property(e => e.SalePrice).HasColumnName("sale_price");
            entity.Property(e => e.Sold).HasColumnName("sold");
            entity.Property(e => e.Status)
                .HasColumnType("bit(1)")
                .HasColumnName("status");

            entity.HasOne(d => d.Brand).WithMany(p => p.ProductEntities)
                .HasForeignKey(d => d.BrandId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FKj94dnak6ecn8sf4oc862mndw3");

            entity.HasOne(d => d.Category).WithMany(p => p.ProductEntities)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK8kxxmqdokh3lthvw0t148w0bc");
        });

        modelBuilder.Entity<ProductSpecEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("product_spec_entity");

            entity.HasIndex(e => e.ProductId, "FK3r5d73df73gohcix5pab4wdqu");

            entity.HasIndex(e => e.ProductAttributeId, "FK66y2bqmh0ngw6s047w2nngtmj");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.ProductAttributeId).HasColumnName("product_attribute_id");
            entity.Property(e => e.ProductId).HasColumnName("product_id");
            entity.Property(e => e.Value)
                .HasMaxLength(255)
                .HasColumnName("value");

            entity.HasOne(d => d.ProductAttribute).WithMany(p => p.ProductSpecEntities)
                .HasForeignKey(d => d.ProductAttributeId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK66y2bqmh0ngw6s047w2nngtmj");

            entity.HasOne(d => d.Product).WithMany(p => p.ProductSpecEntities)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK3r5d73df73gohcix5pab4wdqu");
        });

        modelBuilder.Entity<UserEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("user_entity");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.Email)
                .HasMaxLength(255)
                .HasColumnName("email");
            entity.Property(e => e.FullName)
                .HasMaxLength(255)
                .HasColumnName("full_name");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.Password)
                .HasMaxLength(255)
                .HasColumnName("password");
            entity.Property(e => e.Role)
                .HasMaxLength(255)
                .HasColumnName("role");
            entity.Property(e => e.Status)
                .HasColumnType("bit(1)")
                .HasColumnName("status");
        });

        modelBuilder.Entity<ProductImageEntity>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("product_image_entity");
            entity.HasIndex(e => e.ProductId, "FK3r5d73df73gohcix5pab4wdqw");
            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CreatedAt)
                .HasMaxLength(6)
                .HasColumnName("created_at");
            entity.Property(e => e.DeletedAt)
                .HasMaxLength(6)
                .HasColumnName("deleted_at");
            entity.Property(e => e.LastUpdatedAt)
                .HasMaxLength(6)
                .HasColumnName("last_updated_at");
            entity.Property(e => e.ProductId).HasColumnName("product_id");
            entity.Property(e => e.Name)
                .HasMaxLength(255)
                .HasColumnName("name");
            entity.HasOne(d => d.Product).WithMany(p => p.ProductImageEntities)
                .HasForeignKey(d => d.ProductId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK3r5d73df73gohcix5pab4wdqw");

        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
