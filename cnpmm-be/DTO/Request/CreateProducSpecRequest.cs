namespace cnpmm_be.DTO.Request
{
    public class CreateProducSpecRequest
    {
        public string? Value {get; set;}
        public string? ProductAttributeId {get; set;}
    }
}