namespace cnpmm_be.DTO.Request
{
    public class CreateProductRequest
    {
        public string? Color { get; set; }
        public string? Description { get; set; }
        public string? Guarantee { get; set; }
        public string? Name { get; set; }
        public bool? OnSale {get; set;}
        public double? Price { get; set; }
        public int? Quantity { get; set; }
        public double? SalePrice { get; set; }
        public string? BrandId { get; set; }
        public string? CategoryId { get; set; } 
    }
}