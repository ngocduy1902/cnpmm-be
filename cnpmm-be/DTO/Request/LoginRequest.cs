using System.ComponentModel.DataAnnotations;

namespace cnpmm_be.DTO.Request
{
    public class LoginRequest
    {
        [Required]
        [MaxLength(50)]
        public string? Email { get; set; }
        [Required]
        [MaxLength(50)]
        public string? Password { get; set; }
    }
}