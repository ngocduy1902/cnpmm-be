namespace cnpmm_be.DTO.Request
{
    public class UpdateProfileRequest
    {
        public string? FullName {get; set;}
        public string? Email {get; set;}
    }
}