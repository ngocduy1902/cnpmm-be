using System.Text.Json.Serialization;

namespace cnpmm_be.DTO.Request
{
    public class UpdateOrderStatusRequest
    {
        [JsonIgnore]
        public string? Id {get; set;}
        public string? Status {get; set;}
    }
}