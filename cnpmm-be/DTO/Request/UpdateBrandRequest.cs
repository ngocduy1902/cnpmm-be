using System.Text.Json.Serialization;

namespace cnpmm_be.DTO.Request
{
    public class UpdateBrandRequest
    {
        [JsonIgnore]
        public string? Id {get; set;}
        public IFormFile? Image {get; set;}
        public string? Name {get; set;}
    }
}