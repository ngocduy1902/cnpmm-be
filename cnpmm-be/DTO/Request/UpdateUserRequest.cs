using System.Text.Json.Serialization;

namespace cnpmm_be.DTO.Request
{
    public class UpdateUserRequest
    {
        [JsonIgnore]
        public string? Id { get; set; }

        public bool? Status { get; set; }

    }
}