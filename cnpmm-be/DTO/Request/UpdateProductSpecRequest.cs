namespace cnpmm_be.DTO.Request
{
    public class UpdateProductSpecRequest
    {
        public string Id {get; set;} = null!;
        public string? Value {get; set;}
        public string? ProductAttributeId {get; set;}
    }
}