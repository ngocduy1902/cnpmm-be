using System.Text.Json.Serialization;

namespace cnpmm_be.DTO.Request
{
    public class CreateCategoryRequest
    {
        public string? Name { get; set; }
        public string? Description {get; set;}
    }
}