namespace cnpmm_be.DTO.Request
{
    public class CreateOrderItemRequest
    {
        public string? ProductId {get; set;}
        public int? Quantity {get; set;}
    }
}