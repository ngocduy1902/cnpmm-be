namespace cnpmm_be.DTO.Request
{
    public class CreateBrandRequest
    {
        public string? Name {get; set;}

        public IFormFile? Image {get; set;}
    }
}