namespace cnpmm_be.DTO.Request
{
    public class CreateOrderRequest
    {
        public string? ReceiverName {get; set;}
        public string? ReceiverPhone {get; set;}
        public string? Address {get; set;}
        public string? Payment {get; set;}
        public List<CreateOrderItemRequest>? OrderItemList {get; set;}
    }
}