using System.Text.Json.Serialization;

namespace cnpmm_be.DTO.Request
{
    public class UpdateCategoryRequest
    {
        [JsonIgnore]
        public string? Id {get; set;}
        public string? Name {get; set;}
        public string? Description {get; set;}
    }
}