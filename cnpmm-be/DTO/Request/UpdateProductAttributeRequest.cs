namespace cnpmm_be.DTO.Request
{
    public class UpdateProductAttributeRequest
    {
        public string? Id {get; set;}
        public string? Name {get; set;}
    }
}