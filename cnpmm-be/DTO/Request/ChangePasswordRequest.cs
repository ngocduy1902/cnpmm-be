using System.ComponentModel.DataAnnotations;

namespace cnpmm_be.DTO.Request
{
    public class ChangePasswordRequest
    {
        [Required]
        [MaxLength(50)]
        public string? Password { get; set; }
        [Required]
        [MaxLength(50)]
        public string? NewPassword { get; set; }
    }
}