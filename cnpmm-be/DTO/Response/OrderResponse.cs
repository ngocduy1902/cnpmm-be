namespace cnpmm_be.DTO.Response
{
    public class OrderResponse
    {
        public string? Id {get; set;}
        public string? Status {get; set;}
        public string? CreatedAt {get; set;}
        public double? Total {get; set;}
        public List<OrderItemResponse>? OrderItemList {get; set;}
    }
}