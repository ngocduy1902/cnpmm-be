namespace cnpmm_be.DTO.Response
{
    public class OrderItemResponse
    {
        public string? Id {get; set;}
        public string? ProductId {get; set;}
        public string? ProductName {get; set;}
        public string? ProductImage {get; set;}
        public double? ProductPrice {get; set;}
        public int? Quantity {get; set;}
    }
}