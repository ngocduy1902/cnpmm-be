namespace cnpmm_be.DTO.Response
{
    public class ProductResponse
    {
        public string Id { get; set; } = null!;
        public string? Color { get; set; }
        public string? Description { get; set; }
        public string? Guarantee { get; set; }
        public string? Name { get; set; }
        public bool? OnSale {get; set;}
        public double? Price { get; set; }
        public int? Quantity { get; set; }
        public double? SalePrice { get; set; }
        public int? Sold { get; set; }
        public bool? Status { get; set; }
        public string? BrandId { get; set; }
        public string? CategoryId { get; set; } 
        public List<string>? Images {get; set;}
        public List<ProductSpecResponse>? productSpecs {get; set;}
    }
}