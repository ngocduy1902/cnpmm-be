namespace cnpmm_be.DTO.Response
{
    public class ProductSpecResponse
    {
        public string? Id {get; set;}
        public string? Value {get; set;}
        public string? ProductAttributeId {get; set;}
    }
}