namespace cnpmm_be.DTO.Response
{
    public class CategoryResponse
    {
        public string Id { get; set; } = null!;
        public string? Image { get; set; }
        public string? Name { get; set; }
        public string? Description {get; set;}
        public List<ProductAttributeResponse>? productAttributes {get; set;}
    }
}