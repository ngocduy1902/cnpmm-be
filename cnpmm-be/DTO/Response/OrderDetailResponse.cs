namespace cnpmm_be.DTO.Response
{
    public class OrderDetailResponse
    {
        public string? Id {get; set;}
        public string? CreatedAt {get; set;}
        public string? LastUpdatedAt {get; set;}
        public string? Status {get; set;}
        public double? Total {get; set;}
        public string? ReceiverName {get; set;}
        public string? ReceiverPhone {get; set;}
        public string? Address {get; set;}
        public string? Payment {get; set;}
        public bool? IsPaid {get; set;}

        public List<OrderItemResponse>? OrderItemList {get; set;}   
    }
}