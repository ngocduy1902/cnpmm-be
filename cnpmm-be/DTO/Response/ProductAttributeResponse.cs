namespace cnpmm_be.DTO.Response
{
    public class ProductAttributeResponse
    {
        public string? Id {get; set;}
        public string? Name {get; set;}
    }
}