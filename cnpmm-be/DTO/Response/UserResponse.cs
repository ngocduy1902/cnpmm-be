namespace cnpmm_be.DTO.Response
{
    public class UserResponse
    {
        public string? Id {get; set;}
        public string? Email {get; set;}
        public string? FullName {get; set;}
        public string? Role {get; set;}
        public bool? Status {get; set;}
    }
}