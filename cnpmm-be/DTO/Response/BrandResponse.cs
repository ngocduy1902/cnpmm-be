using cnpmm_be.Entities;

namespace cnpmm_be.DTO.Response
{
    public class BrandResponse
    {
        public string? Id {get; set;}

        public string? Image {get; set;}

        public string? Name {get; set;}
    }
}