namespace cnpmm_be.DTO.Response
{
    public class PaginationResponse
    {
        public object? Content {get; set;}
        public int? CurrentPage {get; set;}
        public int? PageSize {get; set;}
        public int? Total {get; set;}
        public int? TotalPages {get; set;}
    }
}