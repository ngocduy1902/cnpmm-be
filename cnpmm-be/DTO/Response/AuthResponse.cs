namespace cnpmm_be.DTO.Response
{
    public class AuthResponse
    {
        public string? AccessToken {get; set;}
        public UserResponse? User {get; set;}
    }
}