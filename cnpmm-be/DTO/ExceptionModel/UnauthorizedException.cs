namespace cnpmm_be.DTO.ExceptionModel
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(string message) : base(message) {}
    }
}