namespace cnpmm_be.DTO.ExceptionModel
{
    public class ForbiddenException : Exception
    {
        public ForbiddenException(string message) : base(message) {}   
    }
}