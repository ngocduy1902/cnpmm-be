namespace cnpmm_be.DTO.ExceptionModel
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message) {}
    }
}