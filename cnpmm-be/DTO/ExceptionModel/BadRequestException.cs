namespace cnpmm_be.DTO.ExceptionModel
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string message) : base(message) {}
    }
}