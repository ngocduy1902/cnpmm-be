using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers
{
    [ApiController]
    [Route("api/category")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService) {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public IActionResult GetAllCategory() {
            return Ok(categoryService.GetAllCategory());
        }

        [HttpGet("{id}")]
        public IActionResult GetCategoryById(string id) {
            return Ok(categoryService.GetCategoryById(id));
        }
    }
}