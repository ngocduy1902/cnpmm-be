using cnpmm_be.DTO.Request;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace cnpmm_be.Controllers.admin
{
    [ApiController]
    [Route("api/admin/category")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService) {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public IActionResult GetAllCategory() {
            return Ok(categoryService.GetAllCategory());
        }

        [HttpPost]
        public IActionResult CreateCategory([FromForm] string info, IFormFile image,
            [FromForm] string productAttributesString) {
            CreateCategoryRequest request = JsonConvert.DeserializeObject<CreateCategoryRequest>(info)!;
            List<CreateProductAttributeRequest> productAttributes = 
                JsonConvert.DeserializeObject<List<CreateProductAttributeRequest>>(productAttributesString)!;
            return StatusCode(StatusCodes.Status201Created, categoryService.CreateCategory(request, image, productAttributes));
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateCategory(string id, [FromForm] string info, IFormFile? image, [FromForm] string? productAttributesString) {
            UpdateCategoryRequest request = JsonConvert.DeserializeObject<UpdateCategoryRequest>(info)!;
            request.Id = id;
            List<UpdateProductAttributeRequest> productAttributes = 
                JsonConvert.DeserializeObject<List<UpdateProductAttributeRequest>>(productAttributesString!)!;
            return Ok(categoryService.UpdateCategory(request, image!, productAttributes));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCategory(string id) {
            return Ok(categoryService.DeleteCategory(id));
        }

    }
}