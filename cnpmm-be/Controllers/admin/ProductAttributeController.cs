using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers.admin
{
    [TypeFilter(typeof(AdminAuthorizationFilter))]
    [ApiController]
    [Route("api/admin/product-attribute")]
    public class ProductAttributeController : ControllerBase
    {
        private readonly IProductAttributeService productAttributeService;

        public ProductAttributeController(IProductAttributeService productAttributeService) {
            this.productAttributeService = productAttributeService;
        }

        [HttpGet("by-category")]
        public IActionResult getAllProductAttributeByCategory(string categoryId) {
            return Ok(productAttributeService.getAllProductAttributeByCategory(categoryId));
        }
    }
}