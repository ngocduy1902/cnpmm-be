using cnpmm_be.DTO.Request;
using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers.admin
{
    [TypeFilter(typeof(AdminAuthorizationFilter))]
    [ApiController]
    [Route("api/admin/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;

        public UserController(IUserService userService) {
            this.userService = userService;
        }

        [HttpGet]
        public IActionResult GetAllUser() {
            return Ok(userService.GetAllUser());
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateUserStatus(string id, UpdateUserRequest request) {
            request.Id = id;
            return Ok(userService.UpdateUserStatus(request));
        }
    }
}