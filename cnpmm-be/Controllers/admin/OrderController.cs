using cnpmm_be.DTO.Request;
using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Crypto.Engines;

namespace cnpmm_be.Controllers.admin
{
    [TypeFilter(typeof(AdminAuthorizationFilter))]
    [ApiController]
    [Route("api/admin/order")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService) {
            this.orderService = orderService;
        }

        [HttpGet]
        public IActionResult GetAllOrder() {
            return Ok(orderService.GetAllOrder());
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateOrderStatus(string id, UpdateOrderStatusRequest request) {
            request.Id = id;
            return Ok(orderService.UpdateOrderStatus(request));
        }

        [HttpPatch("paid/{id}")]
        public IActionResult UpdateOrderIsPaid(string id) {
            return Ok(orderService.UpdateOrderIsPaid(id));
        }
    }
}