using cnpmm_be.DTO.Request;
using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace cnpmm_be.Controllers.admin
{
    [TypeFilter(typeof(AdminAuthorizationFilter))]
    [ApiController]
    [Route("api/admin/product")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService) {
            this.productService = productService;
        }

        [HttpPost]
        public IActionResult createProduct([FromForm] string info,
            [FromForm(Name = "images")] List<IFormFile> images,
            [FromForm] string productSpecsString) {
            CreateProductRequest request = JsonConvert.DeserializeObject<CreateProductRequest>(info)!;
            List<CreateProducSpecRequest> productSpecs = JsonConvert.DeserializeObject<List<CreateProducSpecRequest>>(productSpecsString)!;
            return StatusCode(StatusCodes.Status201Created, productService.CreateProduct(request, images, productSpecs));
        }
        
        [HttpGet]
        public IActionResult getAllProduct() {
            return Ok(productService.getAllProduct());
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(string id) {
            return Ok(productService.DeleteProduct(id));
        }

        [HttpPatch]
        public IActionResult UpdateProduct([FromForm] string info,
            [FromForm(Name = "images")] List<IFormFile>? images,
            [FromForm] string? productSpecsString) {
            UpdateProductRequest request = JsonConvert.DeserializeObject<UpdateProductRequest>(info)!;
            List<UpdateProductSpecRequest> productSpecs = JsonConvert.DeserializeObject<List<UpdateProductSpecRequest>>(productSpecsString!)!;
            return StatusCode(StatusCodes.Status201Created, productService.UpdateProduct(request, images, productSpecs));
        }
    }
}