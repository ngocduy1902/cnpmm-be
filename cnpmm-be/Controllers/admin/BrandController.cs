using cnpmm_be.DTO.Request;
using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers.admin
{
    [TypeFilter(typeof(AdminAuthorizationFilter))]
    [ApiController]
    [Route("api/admin/brand")]
    public class BrandController : ControllerBase
    {
        private readonly IBrandService brandService;

        public BrandController(IBrandService brandService) {
            this.brandService = brandService;
        }

        [HttpGet]
        public IActionResult getAllBrands() {
            return Ok(brandService.getAllBrands());
        }

        [HttpPost]
        public IActionResult createBrand([FromForm] CreateBrandRequest request) {
            return StatusCode(StatusCodes.Status201Created, brandService.Create(request));
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateBrand(string id, [FromForm] UpdateBrandRequest request) {
            request.Id = id;
            return Ok(brandService.UpdateBrand(request));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteBrand(string id) {
            return Ok(brandService.DeleteBrand(id));
        }
    }
}