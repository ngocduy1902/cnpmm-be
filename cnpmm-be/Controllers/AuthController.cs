using cnpmm_be.DTO.Request;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly IUserService userService;

        public AuthController(IUserService userService) {
            this.userService = userService;
        }
        
        [HttpPost("login")]
        public IActionResult Login(LoginRequest request) {
            return Ok(userService.Login(request));
        }

        [HttpPost("register")]
        public IActionResult Register(RegisterRequest request) {
            return Ok(userService.RegisterUser(request));
        }

        [HttpPatch("forgot-password")]
        public IActionResult ForgotPassword(string email) {
            return Ok(userService.ResetPassword(email));
        }
    }
}