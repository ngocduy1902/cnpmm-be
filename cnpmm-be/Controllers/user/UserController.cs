using cnpmm_be.DTO.Request;
using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers.user
{
    [TypeFilter(typeof(AuthenticationFilter))]
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;

        public UserController(IUserService userService) {
            this.userService = userService;
        }

        [HttpGet]
        public IActionResult GetUserProfile() {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "uid")!.Value; 
            return Ok(userService.GetUserProfile(userId));
        }

        [HttpPatch("change-password")]
        public IActionResult ChangePassword(ChangePasswordRequest request) {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "uid")!.Value;
            return Ok(userService.ChangePassword(request, userId));
        }

        [HttpPatch]
        public IActionResult UpdateProfile(UpdateProfileRequest request) {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "uid")!.Value;
            return Ok(userService.UpdateProfile(userId, request));
        }

    }
}