using System.Net;
using cnpmm_be.DTO.Request;
using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers.user
{
    [TypeFilter(typeof(UserAuthorizationFilter))]
    [ApiController]
    [Route("api/user/order")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService) {
            this.orderService = orderService;
        }

        [HttpPost]
        public IActionResult CreateOrder(CreateOrderRequest request) {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "uid")!.Value; 
            return StatusCode(StatusCodes.Status201Created, orderService.CreateOrder(userId, request));
        }

        [HttpGet]
        public IActionResult GetAllOrder() {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "uid")!.Value; 
            return Ok(orderService.GetOrderByUser(userId));
        }
        
        [HttpGet("{id}")]
        public IActionResult GetOrderDetail(string id) {
            return Ok(orderService.GetOrderDetail(id));
        }

        [HttpPatch("{id}")]
        public IActionResult CancelOrder(string id, UpdateOrderStatusRequest request) {
            request.Id = id;
            return Ok(orderService.CancelOrder(request));
        }
    }
}