using cnpmm_be.Middlewares;
using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers
{   
    [TypeFilter(typeof(AdminAuthorizationFilter))]
    [ApiController]
    [Route("api/order/paid")]
    public class UpdateOrderPaidController : ControllerBase
    {
        private readonly IOrderService orderService;

        public UpdateOrderPaidController(IOrderService orderService) {
            this.orderService = orderService;
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateOrderIsPaid(string id) {
            return Ok(orderService.UpdateOrderIsPaid(id));
        }
    }
}