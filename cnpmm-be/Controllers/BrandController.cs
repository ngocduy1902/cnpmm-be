using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers
{
    [ApiController]
    [Route("api/brand")]
    public class BrandController : ControllerBase
    {
        private readonly IBrandService brandService;

        public BrandController(IBrandService brandService) {
            this.brandService = brandService;
        }

        [HttpGet]
        public IActionResult getAllBrands() {
            return Ok(brandService.getAllBrands());
        }

        [HttpGet("{id}")]
        public IActionResult getBrandById(string id) {
            return Ok(brandService.getBrandById(id));
        }
    }
}