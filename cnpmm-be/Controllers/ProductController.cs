using cnpmm_be.Services;
using Microsoft.AspNetCore.Mvc;

namespace cnpmm_be.Controllers
{
    [ApiController]
    [Route("api/product")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService) {
            this.productService = productService;
        }

        [HttpGet]
        public IActionResult getAllProductPage(int currentPage=0, int pageSize=12) {
            return Ok(productService.getAllProductPage(currentPage, pageSize));
        }

        [HttpGet("by-brand")]
        public IActionResult getAllProductByBrandName(string brandName, int currentPage=0, int pageSize=12) {
            return Ok(productService.getAllProductByBrandName(brandName, currentPage, pageSize));
        }

        [HttpGet("search-filter")]
        public IActionResult searchFilterProduct([FromQuery] Dictionary<string, string> queries) {
            return Ok(productService.SearchFilterProduct(queries));
        }

        [HttpGet("{id}")]
        public IActionResult getProductDetail(string id) {
            return Ok(productService.GetProductDetail(id));
        }

        [HttpGet("by-category")]
        public IActionResult getAllProductByCategory(string categoryName, int currentPage=0, int pageSize=12) {
            return Ok(productService.getAllProductByCategoryName(categoryName, currentPage, pageSize));
        }

        [HttpGet("color/by-category")]
        public IActionResult getAllProductColorByCategory(string categoryId) {
            return Ok(productService.GetProductColorByCategory(categoryId));
        }
    }
}