using System.Text;
using System.Text.Json.Serialization;
using cnpmm_be.Entities;
using cnpmm_be.Repositories;
using cnpmm_be.Repositories.Impl;
using cnpmm_be.Services;
using cnpmm_be.Services.Impl;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace cnpmm_be
{
    public static class ServiceConfiguration
    {
        
        public static void ConfigureMySQLContext(this IServiceCollection service, IConfiguration configuration)
        {
            var host = Environment.GetEnvironmentVariable("DBHOST") ?? "localhost";
            var port = Environment.GetEnvironmentVariable("DBPORT") ?? "3306";
            var password = Environment.GetEnvironmentVariable("MYSQL_PASSWORD") ?? configuration.GetConnectionString("MYSQL_PASSWORD");
            var userid = Environment.GetEnvironmentVariable("MYSQL_USER") ?? configuration.GetConnectionString("MYSQL_USER");
            var database = Environment.GetEnvironmentVariable("MYSQL_DATABASE") ?? configuration.GetConnectionString("MYSQL_DATABASE");
            string connString = $"server={host};port={port};database={database};uid={userid};password={password}";
            service.AddDbContext<EcommercedbContext>(
                opt => opt.UseMySql(connString, Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.1.0-mysql"))
            );
        }

        public static void RegisterDI(this IServiceCollection service) {
            service.AddScoped<IGoogleCloudStorageService>(provider => {
                var projectId = "mtsestorage";
                var jsonFileName = "gcs.json";
                var bucketName = "mtsestorage.appspot.com";
                return new GoogleCloudStorageService(projectId, jsonFileName, bucketName);
            });
            service.AddScoped<IBrandRepo, BrandRepo>();
            service.AddScoped<ICategoryRepo, CategoryRepo>();
            service.AddScoped<IOrderRepo, OrderRepo>();
            service.AddScoped<IOrderItemRepo, OrderItemRepo>();
            service.AddScoped<IProductAttributeRepo, ProductAttributeRepo>();
            service.AddScoped<IProductRepo, ProductRepo>();
            service.AddScoped<IProductSpecRepo, ProductSpecRepo>();
            service.AddScoped<IUserRepo, UserRepo>();
            service.AddScoped<IProductImageRepo, ProductImageRepo>();
            service.AddScoped<IBrandService, BrandService>();
            service.AddScoped<IUserService, UserService>();
            service.AddScoped<IProductService, ProductService>();
            service.AddScoped<IProductSpecService, ProductSpecService>();
            service.AddScoped<IProductAttributeService, ProductAttributeService>();
            service.AddScoped<IOrderService, OrderService>();
            service.AddScoped<ICategoryService, CategoryService>();
            service.AddScoped<IEmailSenderService, EmailSenderService>();
        }

        public static void RegisterCORS(this IServiceCollection service)
        {
            service.AddCors(p => p.AddPolicy("MyCors", build =>
            {
                build.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
            }));
        }

        public static void ConfigureAuthentication(this IServiceCollection service, IConfiguration configuration)
        {
            var secretKey = configuration["AppSettings:SecretKey"];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey!);

            service.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opt => {
                opt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secretKeyBytes),
                    ClockSkew = TimeSpan.Zero,
                };
            });
        }

        public static void ConfigureNewtonsoftJson(this IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddControllers().AddJsonOptions(x =>
                            x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
        }
    }
}