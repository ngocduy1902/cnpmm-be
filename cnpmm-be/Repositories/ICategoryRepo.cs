using cnpmm_be.Entities;

namespace cnpmm_be.Repositories
{
    public interface ICategoryRepo : IBaseRepo<CategoryEntity>
    {
         
    }
}