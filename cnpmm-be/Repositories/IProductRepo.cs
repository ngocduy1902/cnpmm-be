using cnpmm_be.Entities;

namespace cnpmm_be.Repositories
{
    public interface IProductRepo : IBaseRepo<ProductEntity>
    {
         
    }
}