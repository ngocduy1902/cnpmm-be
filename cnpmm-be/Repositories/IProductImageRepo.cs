using cnpmm_be.Entities;

namespace cnpmm_be.Repositories
{
    public interface IProductImageRepo : IBaseRepo<ProductImageEntity>
    {
        
    }
}