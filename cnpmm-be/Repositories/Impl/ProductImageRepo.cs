using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class ProductImageRepo : BaseRepo<ProductImageEntity>, IProductImageRepo
    {
        public ProductImageRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}