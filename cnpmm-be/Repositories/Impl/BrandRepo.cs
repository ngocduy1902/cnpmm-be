using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class BrandRepo : BaseRepo<BrandEntity>, IBrandRepo
    {
        public BrandRepo(EcommercedbContext context) : base(context){}
    }
}