using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class ProductRepo : BaseRepo<ProductEntity>, IProductRepo
    {
        public ProductRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}