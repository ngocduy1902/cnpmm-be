using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class ProductSpecRepo : BaseRepo<ProductSpecEntity>, IProductSpecRepo
    {
        public ProductSpecRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}