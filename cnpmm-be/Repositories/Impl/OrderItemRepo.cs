using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class OrderItemRepo : BaseRepo<OrderItemEntity>, IOrderItemRepo
    {
        public OrderItemRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}