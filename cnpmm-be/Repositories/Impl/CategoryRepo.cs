using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class CategoryRepo : BaseRepo<CategoryEntity>, ICategoryRepo
    {
        public CategoryRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}