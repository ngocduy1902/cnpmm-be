using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class ProductAttributeRepo : BaseRepo<ProductAttributeEntity>, IProductAttributeRepo
    {
        public ProductAttributeRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}