using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class OrderRepo : BaseRepo<OrderEntity>, IOrderRepo
    {
        public OrderRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}