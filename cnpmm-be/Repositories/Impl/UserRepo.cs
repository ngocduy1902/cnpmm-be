using cnpmm_be.Entities;

namespace cnpmm_be.Repositories.Impl
{
    public class UserRepo : BaseRepo<UserEntity>, IUserRepo
    {
        public UserRepo(EcommercedbContext context) : base(context)
        {
        }
    }
}