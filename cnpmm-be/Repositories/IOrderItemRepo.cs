using cnpmm_be.Entities;

namespace cnpmm_be.Repositories
{
    public interface IOrderItemRepo : IBaseRepo<OrderItemEntity>
    {
         
    }
}