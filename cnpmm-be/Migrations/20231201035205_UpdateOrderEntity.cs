﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace cnpmm_be.Migrations
{
    /// <inheritdoc />
    public partial class UpdateOrderEntity : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "address",
                table: "order_entity",
                type: "varchar(255)",
                maxLength: 255,
                nullable: true,
                collation: "utf8mb4_0900_ai_ci")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<ulong>(
                name: "is_paid",
                table: "order_entity",
                type: "bit(1)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "payment",
                table: "order_entity",
                type: "varchar(255)",
                maxLength: 255,
                nullable: true,
                collation: "utf8mb4_0900_ai_ci")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "receiver_name",
                table: "order_entity",
                type: "varchar(255)",
                maxLength: 255,
                nullable: true,
                collation: "utf8mb4_0900_ai_ci")
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<string>(
                name: "receiver_phone",
                table: "order_entity",
                type: "varchar(255)",
                maxLength: 255,
                nullable: true,
                collation: "utf8mb4_0900_ai_ci")
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "address",
                table: "order_entity");

            migrationBuilder.DropColumn(
                name: "is_paid",
                table: "order_entity");

            migrationBuilder.DropColumn(
                name: "payment",
                table: "order_entity");

            migrationBuilder.DropColumn(
                name: "receiver_name",
                table: "order_entity");

            migrationBuilder.DropColumn(
                name: "receiver_phone",
                table: "order_entity");
        }
    }
}
