namespace cnpmm_be.Base
{
    public class ExceptionResponse : ResponseBaseAbstract
    {
        public ExceptionResponse() {
            base.IsSuccess = false;
        }
    }
}