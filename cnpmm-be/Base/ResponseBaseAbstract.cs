namespace cnpmm_be.Base
{
    public abstract class ResponseBaseAbstract
    {
        public bool? IsSuccess {get; set;}

        public object? Data {get; set;}

        public string? Message {get; set;}

        public ResponseBaseAbstract() {
            this.IsSuccess = true;
            this.Data = null;
            this.Message = "";
        }
    }
}