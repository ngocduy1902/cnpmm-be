using AutoMapper;
using cnpmm_be.DTO.ExceptionModel;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Entities;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepo orderRepo;

        private readonly IOrderItemRepo orderItemRepo;

        private readonly IUserRepo userRepo;

        private readonly IProductRepo productRepo;

        private readonly IGoogleCloudStorageService storageService;

        private readonly string[] orderStatus = new string[] {"Đã tạo", "Đã xác nhận", "Đang giao", "Đã giao", "Đã hủy"};

        private readonly IMapper mapper = new MapperConfiguration(cfg => {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();

        public OrderService(IOrderRepo orderRepo, IOrderItemRepo orderItemRepo, IUserRepo userRepo,
            IProductRepo productRepo, IGoogleCloudStorageService storageService) {
            this.orderRepo = orderRepo;
            this.orderItemRepo = orderItemRepo;
            this.userRepo = userRepo;
            this.productRepo = productRepo;
            this.storageService = storageService;
        }

        public List<OrderDetailResponse> GetAllOrder() {
            var orderList = orderRepo.FindAll(o => o.OrderItemEntities);
            List<OrderDetailResponse> responses = new List<OrderDetailResponse>();
            foreach (var order in orderList) {
                OrderDetailResponse response = mapper.Map<OrderDetailResponse>(order);
                List<OrderItemResponse> orderItemResponses = new List<OrderItemResponse>();
                foreach (var orderItemEntity in order.OrderItemEntities) {
                    OrderItemResponse orderItemResponse = mapper.Map<OrderItemResponse>(orderItemEntity);
                    ProductEntity product = productRepo.FindById(orderItemEntity.ProductId!, p => p.ProductImageEntities);
                    ProductImageEntity productImage = product.ProductImageEntities.First();
                    orderItemResponse.ProductImage = storageService.GetImageUrl(productImage.Name!);
                    orderItemResponses.Add(orderItemResponse);
                }
                response.OrderItemList = orderItemResponses;
                responses.Add(response);
            }
            return responses;
        }

        public OrderResponse CreateOrder(string userId, CreateOrderRequest request) {
            double? total = 0;
            foreach (var orderItemRequest in request.OrderItemList!) {
                var product = productRepo.FindById(orderItemRequest.ProductId!);
                if (product == null)
                    throw new NotFoundException($"Khong tim thay product voi id = {orderItemRequest.ProductId}");
                var price = product.SalePrice != 0 ? product.SalePrice : product.Price;
                total += price * orderItemRequest.Quantity;
            }

            OrderEntity order = new OrderEntity() {
                Id = Guid.NewGuid().ToString(),
                UserId = userId,
                Total = total,
                Status = "Đã tạo",
                ReceiverName = request.ReceiverName,
                ReceiverPhone = request.ReceiverPhone,
                Address = request.Address,
                IsPaid = false,
                Payment = request.Payment

            };
            orderRepo.Create(order);
            orderRepo.Save();
            List<OrderItemResponse> orderItemResponses = new List<OrderItemResponse>();
            foreach (CreateOrderItemRequest orderItem in request.OrderItemList!) {
                OrderItemEntity orderItemEntity = mapper.Map<OrderItemEntity>(orderItem);
                orderItemEntity.Id = Guid.NewGuid().ToString();
                orderItemEntity.Order = order;
                ProductEntity productEntity = productRepo.FindById(orderItem.ProductId!, p => p.ProductImageEntities);
                if (productEntity == null)
                    throw new NotFoundException($"Khong tim thay san pham voi id = {orderItem.ProductId!}");
                orderItemEntity.ProductId = orderItem.ProductId!;
                orderItemEntity.ProductName = productEntity.Name;
                orderItemEntity.ProductPrice = productEntity.SalePrice != 0 ? productEntity.SalePrice : productEntity.Price;
                orderItemRepo.Create(orderItemEntity);
                orderItemRepo.Save();
                productEntity.Quantity -= orderItem.Quantity;
                productEntity.Sold += orderItem.Quantity;
                productRepo.Update(productEntity);
                productRepo.Save();
                OrderItemResponse orderItemResponse = mapper.Map<OrderItemResponse>(orderItemEntity);
                ProductImageEntity productImage = productEntity.ProductImageEntities.First();
                orderItemResponse.ProductImage = storageService.GetImageUrl(productImage.Name!);
                orderItemResponses.Add(orderItemResponse);
            }
            OrderResponse orderResponse = mapper.Map<OrderResponse>(order);
            orderResponse.OrderItemList = orderItemResponses;
            return orderResponse;
        }

        public List<OrderResponse> GetOrderByUser(string userId) {
            var user = userRepo.FindById(userId);
            if (user == null)
                throw new NotFoundException($"Khong tim thay nguoi dung voi id = {userId}");
        
            List<OrderEntity> orderEntities = orderRepo.FindByCondition(o => o.User == user, o => o.OrderItemEntities);
            List<OrderResponse> orderResponses = new List<OrderResponse>();
            foreach (var orderEntity in orderEntities) {
                OrderResponse orderResponse = mapper.Map<OrderResponse>(orderEntity);
                List<OrderItemResponse> orderItemResponses = new List<OrderItemResponse>();
                foreach (var orderItemEntity in orderEntity.OrderItemEntities) {
                    OrderItemResponse orderItemResponse = mapper.Map<OrderItemResponse>(orderItemEntity);
                    ProductEntity product = productRepo.FindById(orderItemEntity.ProductId!, p => p.ProductImageEntities);
                    ProductImageEntity productImage = product.ProductImageEntities.First();
                    orderItemResponse.ProductImage = storageService.GetImageUrl(productImage.Name!);
                    orderItemResponses.Add(orderItemResponse);
                }
                orderResponse.OrderItemList = orderItemResponses;
                orderResponses.Add(orderResponse);
            }
            return orderResponses;
        }

        public OrderDetailResponse GetOrderDetail(string orderId) {
            var orderEntity = orderRepo.FindById(orderId, o => o.OrderItemEntities);
            if (orderEntity == null)
                throw new NotFoundException($"Khong tim thay don hang voi id = {orderId}");
            OrderDetailResponse response = mapper.Map<OrderDetailResponse>(orderEntity);
            List<OrderItemResponse> orderItemResponses = new List<OrderItemResponse>();
            foreach (var orderItemEntity in orderEntity.OrderItemEntities) {
                OrderItemResponse orderItemResponse = mapper.Map<OrderItemResponse>(orderItemEntity);
                ProductEntity product = productRepo.FindById(orderItemEntity.ProductId!, p => p.ProductImageEntities);
                ProductImageEntity productImage = product.ProductImageEntities.First();
                orderItemResponse.ProductImage = storageService.GetImageUrl(productImage.Name!);
                orderItemResponses.Add(orderItemResponse);
            }
            response.OrderItemList = orderItemResponses;
            return response;
        }

        public OrderDetailResponse UpdateOrderStatus(UpdateOrderStatusRequest request) {
            var order = orderRepo.FindById(request.Id!, o => o.OrderItemEntities);
            if (order == null)
                throw new NotFoundException($"Khong tim thay don hang voi id = {request.Id}");
            string status = request.Status!;
            int index = orderStatus.ToList().IndexOf(status);
            if (index == -1)
                throw new BadRequestException("Trang thai don hang khong hop le");
            order.Status = orderStatus[index+1];
            order.LastUpdatedAt = DateTime.Now;
            orderRepo.Update(order);
            orderRepo.Save();
            OrderDetailResponse response = mapper.Map<OrderDetailResponse>(order);
            List<OrderItemResponse> orderItemResponses = new List<OrderItemResponse>();
            foreach (var orderItemEntity in order.OrderItemEntities) {
                OrderItemResponse orderItemResponse = mapper.Map<OrderItemResponse>(orderItemEntity);
                ProductEntity product = productRepo.FindById(orderItemEntity.ProductId!, p => p.ProductImageEntities);
                ProductImageEntity productImage = product.ProductImageEntities.First();
                orderItemResponse.ProductImage = storageService.GetImageUrl(productImage.Name!);
                orderItemResponses.Add(orderItemResponse);
            }
            response.OrderItemList = orderItemResponses;
            return response;
        }

        public string CancelOrder(UpdateOrderStatusRequest request) {
            var order = orderRepo.FindById(request.Id!, o => o.OrderItemEntities);
            if (order == null)
                throw new NotFoundException($"Khong tim thay don hang voi id = {request.Id}");
            string status = request.Status!;
            if (status == "Đang giao" || status == "Đã giao")
                throw new BadRequestException("Trang thai hien tai cua don hang khong cho phep huy");
            order.Status = "Đã hủy";
            order.LastUpdatedAt = DateTime.Now;
            orderRepo.Update(order);
            orderRepo.Save();
            foreach (var orderItem in order.OrderItemEntities) {
                var product = productRepo.FindById(orderItem.ProductId!);
                product.Sold -= orderItem.Quantity;
                product.Quantity += orderItem.Quantity;
                productRepo.Update(product);
                productRepo.Save();
            }
            return "Huy don hang thanh cong";
        }

        public OrderDetailResponse UpdateOrderIsPaid(string id) {
            var order = orderRepo.FindById(id, o => o.OrderItemEntities);
            if (order == null)
                throw new NotFoundException($"Khong tim thay don hang voi id = {id}");
            order.IsPaid = true;
            order.LastUpdatedAt = DateTime.Now;
            orderRepo.Update(order);
            orderRepo.Save();
            OrderDetailResponse response = mapper.Map<OrderDetailResponse>(order);
            List<OrderItemResponse> orderItemResponses = new List<OrderItemResponse>();
            foreach (var orderItemEntity in order.OrderItemEntities) {
                OrderItemResponse orderItemResponse = mapper.Map<OrderItemResponse>(orderItemEntity);
                ProductEntity product = productRepo.FindById(orderItemEntity.ProductId!, p => p.ProductImageEntities);
                ProductImageEntity productImage = product.ProductImageEntities.First();
                orderItemResponse.ProductImage = storageService.GetImageUrl(productImage.Name!);
                orderItemResponses.Add(orderItemResponse);
            }
            response.OrderItemList = orderItemResponses;
            return response;
        }
    }
}