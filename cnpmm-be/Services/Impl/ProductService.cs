using System.Reflection;
using AutoMapper;
using cnpmm_be.DTO.ExceptionModel;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Entities;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class ProductService : IProductService
    {
        private readonly IProductRepo productRepo;
        
        private readonly ICategoryRepo categoryRepo;

        private readonly IBrandRepo brandRepo;

        private readonly IProductImageRepo productImageRepo;

        private readonly IGoogleCloudStorageService storageService;

        private readonly IProductSpecRepo productSpecRepo;

        private readonly IProductSpecService productSpecService;

        private readonly IProductAttributeRepo productAttributeRepo;

        private readonly string INVALID_PARAMETER_MESSAGE = "Tham so khong hop le";

        private readonly IMapper mapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();

        public ProductService(IProductRepo productRepo, ICategoryRepo categoryRepo, IBrandRepo brandRepo,
            IProductImageRepo productImageRepo, IGoogleCloudStorageService storageService,
            IProductSpecRepo productSpecRepo, IProductAttributeRepo productAttributeRepo, IProductSpecService productSpecService) {
            this.productRepo = productRepo;
            this.categoryRepo = categoryRepo;
            this.brandRepo = brandRepo;
            this.productImageRepo = productImageRepo;
            this.storageService = storageService;
            this.productSpecRepo = productSpecRepo;
            this.productAttributeRepo = productAttributeRepo;
            this.productSpecService = productSpecService;
        }

        public ProductResponse CreateProduct(CreateProductRequest request, List<IFormFile> images,
            List<CreateProducSpecRequest> productSpecs) {
            if (request.CategoryId != null)
                if (categoryRepo.FindById(request.CategoryId) == null)
                    throw new BadRequestException($"Khong tim thay category voi id = {request.CategoryId}");
            
            if (request.BrandId != null)
                if (brandRepo.FindById(request.BrandId) == null)
                    throw new BadRequestException($"Khong tim thay brand voi id = {request.BrandId}");
            
            ProductEntity product = mapper.Map<ProductEntity>(request);
            product.Id = Guid.NewGuid().ToString();
            product.Status = true;
            product.Sold = 0;
            productRepo.Create(product);
            productRepo.Save();
            foreach(IFormFile image in images) {
                string name = storageService.Save(image);
                ProductImageEntity productImageEntity = new ProductImageEntity {
                    Id = Guid.NewGuid().ToString(),
                    Name = name,
                    ProductId = product.Id
                };
                productImageRepo.Create(productImageEntity);
                productImageRepo.Save();
            }
            foreach (var productSpec in productSpecs) {
                ProductSpecEntity productSpecEntity = new() {
                  Id = Guid.NewGuid().ToString(),
                  Value = productSpec.Value,
                  ProductId = product.Id,
                  ProductAttributeId = productSpec.ProductAttributeId  
                };
                productSpecRepo.Create(productSpecEntity);
                productSpecRepo.Save();
            }

            ProductResponse response =  mapper.Map<ProductResponse>(product);
            SetProductSpecToProduct(response, product);
            generateImageUrl(response);
            return response;
        }

        public ProductResponse UpdateProduct(UpdateProductRequest request, List<IFormFile>? images,
            List<UpdateProductSpecRequest>? productSpecs) {
            string id = request.Id!;
            var product = productRepo.FindById(id, p => p.ProductImageEntities, p => p.ProductSpecEntities);
            if (product == null)
                throw new NotFoundException($"Khong tim thay san pham voi id = {id}");
            if (request.CategoryId != null)
                if (categoryRepo.FindById(request.CategoryId) == null)
                    throw new BadRequestException($"Khong tim thay category voi id = {request.CategoryId}");
            
            if (request.BrandId != null)
                if (brandRepo.FindById(request.BrandId) == null)
                    throw new BadRequestException($"Khong tim thay brand voi id = {request.BrandId}");
            product = mapper.Map<ProductEntity>(request);
            productRepo.Update(product);
            productRepo.Save();
            var productImages = product.ProductImageEntities.ToArray();
            if (images!.Count != 0) {
                for (int i = 0;i < images!.Count;i++) {
                    storageService.Delete(productImages[i].Name!);
                    string imageName = storageService.Save(images[i]);
                    productImages[i].Name = imageName;
                    productImages[i].LastUpdatedAt = new DateTime();
                    productImageRepo.Update(productImages[i]);
                    productImageRepo.Save();
                }
            }
            if (productSpecs!.Count != 0) {
                foreach (var productSpec in productSpecs) {
                    productSpecService.UpdateProductSpec(productSpec);
                }
            }
            ProductResponse response =  mapper.Map<ProductResponse>(product);
            SetProductSpecToProduct(response, product);
            generateImageUrl(response);
            return response;
        }

        public List<ProductResponse> getAllProduct() {
            List<ProductEntity> productList = productRepo.FindAll(p => p.ProductImageEntities, p => p.ProductSpecEntities);
            List<ProductResponse> productResponses = new List<ProductResponse>();
            foreach (ProductEntity product in productList) {
                ProductResponse response = mapper.Map<ProductResponse>(product);
                SetProductSpecToProduct(response, product);
                productResponses.Add(response);
            }
            foreach(ProductResponse resp in productResponses)
                generateImageUrl(resp);
            return productResponses;
        }

        public PaginationResponse getAllProductPage(int currentPage, int pageSize) {
            int total;
            IQueryable<ProductEntity> query = productRepo.QueryAll(x => x.ProductImageEntities, x => x.ProductSpecEntities).Where(x => x.DeletedAt == null);

            total = query.Count();
            List<ProductEntity> productList = query
                .Skip(currentPage * pageSize)
                .Take(pageSize)
                .ToList();

            List<ProductResponse> productResponses = new List<ProductResponse>();
            foreach (ProductEntity product in productList) {
                ProductResponse response = mapper.Map<ProductResponse>(product);
                SetProductSpecToProduct(response, product);
                productResponses.Add(response);
            }

            foreach(ProductResponse resp in productResponses)
                generateImageUrl(resp);

            return new PaginationResponse {
                Content = productResponses,
                CurrentPage = currentPage,
                PageSize = pageSize,
                Total = total,
                TotalPages = (int)Math.Ceiling(total / (double)pageSize)
            };
        }

        public PaginationResponse getAllProductByBrandName(string brandName, int currentPage, int pageSize) {
            BrandEntity brand = brandRepo.FindByCondition(b => b.Name == brandName).FirstOrDefault()!;
            if (brand == null)
                throw new NotFoundException($"Khong tim thay brand co ten la {brandName}");

            IQueryable<ProductEntity> query = productRepo.QueryAll(x => x.ProductImageEntities, x => x.ProductSpecEntities).Where(x => x.Brand!.Name == brandName && x.DeletedAt == null);
            int total = query.Count();
            List<ProductEntity> productList = query
                .Skip(currentPage * pageSize)
                .Take(pageSize)
                .ToList();

            List<ProductResponse> productResponses = new List<ProductResponse>();
            foreach (ProductEntity product in productList) {
                ProductResponse response = mapper.Map<ProductResponse>(product);
                SetProductSpecToProduct(response, product);
                productResponses.Add(response);
            }

            foreach(ProductResponse resp in productResponses)
                generateImageUrl(resp);

            return new PaginationResponse {
                Content = productResponses,
                CurrentPage = currentPage,
                PageSize = pageSize,
                TotalPages = (int)Math.Ceiling(total / (double)pageSize)
            };
        }

        public PaginationResponse getAllProductByCategoryName(string categoryName, int currentPage, int pageSize) {
            CategoryEntity category = categoryRepo.FindByCondition(b => b.Name == categoryName).FirstOrDefault()!;
            if (category == null)
                throw new NotFoundException($"Khong tim thay category co ten la {categoryName}");

            IQueryable<ProductEntity> query = productRepo.QueryAll(x => x.ProductImageEntities, x => x.ProductSpecEntities).Where(x => x.Category!.Name == categoryName && x.DeletedAt == null);
            int total = query.Count();
            List<ProductEntity> productList = query
                .Skip(currentPage * pageSize)
                .Take(pageSize)
                .ToList();

            List<ProductResponse> productResponses = new List<ProductResponse>();
            foreach (ProductEntity product in productList) {
                ProductResponse response = mapper.Map<ProductResponse>(product);
                SetProductSpecToProduct(response, product);
                productResponses.Add(response);
            }

            foreach(ProductResponse resp in productResponses)
                generateImageUrl(resp);

            return new PaginationResponse {
                Content = productResponses,
                CurrentPage = currentPage,
                PageSize = pageSize,
                TotalPages = (int)Math.Ceiling(total / (double)pageSize)
            };
        }

        public PaginationResponse SearchFilterProduct(Dictionary<string, string> queries) {
            int total;
            IQueryable<ProductEntity> query = productRepo.QueryAll(x => x.ProductImageEntities, x => x.ProductSpecEntities).Where(x => x.DeletedAt == null);
            int currentPage=0;
            int pageSize=0;
            bool asc = true;
            string sortField="";
            foreach (var pair in queries) {
                string key = pair.Key;
                string value = pair.Value;
                if (!(key == "currentPage" || key == "pageSize" || key == "sort")) {
                    string[] tokens = key.Split('/');
                    if (tokens.Length < 2)
                        throw new BadRequestException(INVALID_PARAMETER_MESSAGE);
                    
                    string fieldName = tokens[0];
                    string fieldFilter = tokens[tokens.Length - 1];
                    PropertyInfo field = getField(fieldName);

                    if (field == null)
                        throw new BadRequestException(INVALID_PARAMETER_MESSAGE);

                    switch (fieldFilter)
                    {
                        case "contains":
                            if (!(field.PropertyType == typeof(string)))
                                throw new BadRequestException(INVALID_PARAMETER_MESSAGE);
                            if (fieldName == "Name")
                                query = query.Where(p => p.Name!.Contains(value));
                            else
                                query = query.Where(p => p.Id!.Contains(value));
                            break;
                        
                        case "equals":
                            if (fieldName == "Id")
                                query = query.Where(p => p.Id == value);
                            else if (fieldName == "Name")
                                query = query.Where(p => p.Name == value);
                            else if (fieldName == "Color")
                                query = query.Where(p => p.Color == value);
                            else if (fieldName == "Price")
                                query = query.Where(p => p.Price == double.Parse(value));
                            else if (fieldName == "SalePrice")
                                query = query.Where(p => p.SalePrice == double.Parse(value));
                            else if (fieldName == "BrandId")
                                query = query.Where(p => p.BrandId == value);
                            else if (fieldName == "CategoryId")
                                query = query.Where(p => p.CategoryId == value);
                            break;
                        
                        case "min":
                            if (fieldName == "Price")
                                query = query.Where(p => p.Price >= double.Parse(value));
                            else
                                query = query.Where(p => p.SalePrice >= double.Parse(value));
                            break;
                        case "max":
                            if (fieldName == "Price")
                                query = query.Where(p => p.Price <= double.Parse(value));
                            else
                                query = query.Where(p => p.SalePrice <= double.Parse(value));
                            break;
                        default:
                            throw new BadRequestException(INVALID_PARAMETER_MESSAGE);
                    }
                }
                else if (key == "sort") {
                    if (value == "price_asc") {
                        asc = true;
                        sortField = "price";
                    }
                        
                    else if (value == "price_desc") {
                        asc = false;
                        sortField = "price";
                    }
                    else if (value == "sold") {
                        sortField = "sold";
                    }
                    else
                        throw new BadRequestException(INVALID_PARAMETER_MESSAGE);

                }
                else if (key == "currentPage")
                    currentPage = int.Parse(value);
                else
                    pageSize = int.Parse(value);
            }

            total = query.Count();

            List<ProductEntity> productList = new List<ProductEntity>();

            currentPage = currentPage == 0 ? 0 : currentPage;
            pageSize = pageSize == 0 ? 12 : pageSize;
            
            if (sortField == "price") {
                if (asc) {
                    productList = query
                        .OrderBy(p => p.Price)
                        .Skip(currentPage * pageSize)
                        .Take(pageSize)
                        .ToList();
                }
                else
                    productList = query
                        .OrderByDescending(p => p.Price)
                        .Skip(currentPage * pageSize)
                        .Take(pageSize)
                        .ToList();
            }
            else if (sortField == "sold")
                productList = query
                    .OrderByDescending(p => p.Sold)
                    .Skip(currentPage * pageSize)
                    .Take(pageSize)
                    .ToList();
            else
                productList = query
                    .Skip(currentPage * pageSize)
                    .Take(pageSize)
                    .ToList();
            
            List<ProductResponse> productResponses = new List<ProductResponse>();
            foreach (ProductEntity product in productList) {
                ProductResponse response = mapper.Map<ProductResponse>(product);
                SetProductSpecToProduct(response, product);
                productResponses.Add(response);
            }

            foreach(ProductResponse resp in productResponses)
                generateImageUrl(resp);

            return new PaginationResponse {
                Content = productResponses,
                CurrentPage = currentPage,
                PageSize = pageSize,
                Total = total,
                TotalPages = (int)Math.Ceiling(total / (double)pageSize)
            };
        }

        public ProductResponse GetProductDetail(string id) {
            var product = productRepo.FindById(id, p => p.ProductImageEntities, p => p.ProductSpecEntities);
            if (product == null)
                throw new NotFoundException($"Khong tim thay san pham voi id = {id}");
            ProductResponse response = mapper.Map<ProductResponse>(product);
            SetProductSpecToProduct(response, product);
            generateImageUrl(response);
            return response;
        }

        public string DeleteProduct(string id) {
            var product = productRepo.FindById(id, p => p.ProductImageEntities);
            if (product == null)
                throw new NotFoundException($"Khong tim thay san pham voi id = {id}");
            var producImageList = product.ProductImageEntities;
            foreach (var image in producImageList)
                storageService.Delete(image.Name!);

            productRepo.Delete(product);
            productRepo.Save();
            return "Xoa du lieu thanh cong";
        }

        public List<string> GetProductColorByCategory(string categoryId) {
            var category = categoryRepo.FindById(categoryId);
            if (category == null)
                throw new NotFoundException($"Khong tim thay category co id la {categoryId}");
            IQueryable<ProductEntity> query = productRepo.QueryAll();
            IQueryable<string> query1 = query.Where(p => p.CategoryId == categoryId).Select(p => p.Color).Distinct()!;
            return query1.ToList();
        }

        private PropertyInfo getField(string fieldName) {
            Type entityType = typeof(ProductEntity);
            PropertyInfo[] properties = entityType.GetProperties();
            PropertyInfo field = properties.FirstOrDefault(p => p.Name == fieldName)!;
            return field;
        }

        private void generateImageUrl(ProductResponse response) {
            List<string> imageUrls = new List<string>();
            foreach(string image in response.Images!) {
                string url = storageService.GetImageUrl(image);
                imageUrls.Add(url);
            }
            response.Images = imageUrls;
        }

        private void SetProductSpecToProduct(ProductResponse response, ProductEntity entity) {
            var productSpecs = entity.ProductSpecEntities;
            List<ProductSpecResponse> productSpecResponses = new List<ProductSpecResponse>();
            foreach (var productSpec in productSpecs) {
                ProductSpecResponse productSpecResponse = mapper.Map<ProductSpecResponse>(productSpec);
                productSpecResponses.Add(productSpecResponse);
            }
            response.productSpecs = productSpecResponses;
        }
    }
}