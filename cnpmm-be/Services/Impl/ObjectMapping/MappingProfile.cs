using AutoMapper;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Entities;

namespace cnpmm_be.Services.Impl.ObjectMapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile() {
            CreateMap<BrandEntity, BrandResponse>();
            CreateMap<CreateBrandRequest, BrandEntity>();
            CreateMap<UserEntity, UserResponse>();
            CreateMap<RegisterRequest, UserEntity>();
            CreateMap<UpdateUserRequest, UserEntity>();
            CreateMap<CreateProductRequest, ProductEntity>();
            CreateMap<UpdateProductRequest, ProductEntity>();
            CreateMap<ProductEntity, ProductResponse>()
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.ProductImageEntities.Select(p => p.Name).ToList()));
            CreateMap<CategoryEntity, CategoryResponse>();
            CreateMap<CreateOrderItemRequest, OrderItemEntity>();
            CreateMap<OrderItemEntity, OrderItemResponse>();
            CreateMap<OrderEntity, OrderResponse>()
                .ForMember(dest => dest.CreatedAt, opt => 
                opt.MapFrom(src => src.CreatedAt!.Value.ToString("MM/dd/yyyy H:mm:ss")));
            CreateMap<OrderEntity, OrderDetailResponse>()
                .ForMember(dest => dest.CreatedAt, opt => 
                opt.MapFrom(src => src.CreatedAt!.Value.ToString("MM/dd/yyyy H:mm:ss")))
                .ForMember(dest => dest.LastUpdatedAt, opt => 
                opt.MapFrom(src => src.LastUpdatedAt!.Value.ToString("MM/dd/yyyy H:mm:ss")));
            CreateMap<ProductAttributeEntity, ProductAttributeResponse>();
            CreateMap<ProductSpecEntity, ProductSpecResponse>();
        }
    }
}