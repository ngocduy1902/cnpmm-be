using System.Reflection;
using cnpmm_be.DTO.ExceptionModel;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;

namespace cnpmm_be.Services.Impl
{
    public class GoogleCloudStorageService : IGoogleCloudStorageService
    {
        private readonly StorageClient storageClient;
        private readonly string bucketName;

        public GoogleCloudStorageService(string projectId, string jsonFileName, string bucketName) {
            var appDataPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "AppData");
            var jsonPath = Path.Combine(appDataPath, jsonFileName);

            var credential = GoogleCredential.FromFile(jsonPath);
            storageClient = StorageClient.Create(credential);
            this.bucketName = bucketName;
        }

        public string Save(IFormFile file) {
            if (file == null || file.Length == 0)
                throw new ArgumentNullException("File khong hop le");

            var originalFileName = file.FileName;
            var fileExtension = Path.GetExtension(originalFileName);

            var fileName = $"{Ulid.NewUlid().ToString()}{fileExtension}";
            using (var stream = file.OpenReadStream())
            {
                storageClient.UploadObject(bucketName, fileName, null, stream);
            }

            return fileName;
        }

        public string GetImageUrl(string fileName) {
            return $"https://storage.googleapis.com/{bucketName}/{fileName}";

        }

        public void Delete(string fileName)
        {   
            try {
                storageClient.DeleteObject(bucketName, fileName);
            }
            catch(Google.GoogleApiException ex) when (ex.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new BadRequestException($"Khong tim thay file voi fileName = {fileName}");
            }
            
        }
    }
}