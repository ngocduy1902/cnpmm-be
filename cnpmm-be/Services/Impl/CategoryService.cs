using AutoMapper;
using cnpmm_be.DTO.ExceptionModel;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Entities;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepo categoryRepo;

        private readonly IGoogleCloudStorageService storageService;

        private readonly IProductAttributeRepo productAttributeRepo;

        private readonly IMapper mapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();

        public CategoryService(ICategoryRepo categoryRepo, IGoogleCloudStorageService storageService,
            IProductAttributeRepo productAttributeRepo) {
            this.categoryRepo = categoryRepo;
            this.storageService = storageService;
            this.productAttributeRepo = productAttributeRepo;
        }

        public List<CategoryResponse> GetAllCategory() {
            List<CategoryEntity> categoryList = categoryRepo.FindAll(c => c.ProductAttributeEntities);
            List<CategoryResponse> responses = new List<CategoryResponse>();
            foreach (CategoryEntity category in categoryList) {
                CategoryResponse response = mapper.Map<CategoryResponse>(category);
                var productAttributes = category.ProductAttributeEntities;
                List<ProductAttributeResponse> productAttributeResponses = new List<ProductAttributeResponse>();
                foreach (var productAttribute in productAttributes) {
                    var productAttributeResponse = mapper.Map<ProductAttributeResponse>(productAttribute);
                    productAttributeResponses.Add(productAttributeResponse);
                }
                response.productAttributes = productAttributeResponses;
                GenerateImageUrl(response);
                responses.Add(response);
            }
            return responses;
        }

        public CategoryResponse GetCategoryById(string id) {
            var category = categoryRepo.FindById(id, c => c.ProductAttributeEntities);
            if (category == null)
                throw new NotFoundException($"Khong tim thay category voi id = {id}");
            CategoryResponse response = mapper.Map<CategoryResponse>(category);
            var productAttributes = category.ProductAttributeEntities;
            List<ProductAttributeResponse> productAttributeResponses = new List<ProductAttributeResponse>();
            foreach (var productAttribute in productAttributes) {
                var productAttributeResponse = mapper.Map<ProductAttributeResponse>(productAttribute);
                productAttributeResponses.Add(productAttributeResponse);
            }
            response.productAttributes = productAttributeResponses;
            GenerateImageUrl(response);
            return response;
        }

        public CategoryResponse CreateCategory(CreateCategoryRequest request, IFormFile image, List<CreateProductAttributeRequest> productAttributes) {
            CategoryEntity category = new()
            {
                Id = Guid.NewGuid().ToString(),
                Name = request.Name,
                Image = storageService.Save(image),
                Description = request.Description
            };
            categoryRepo.Create(category);
            categoryRepo.Save();
            List<ProductAttributeResponse> productAttrResponses = new List<ProductAttributeResponse>();
            foreach (var productAttribute in productAttributes) {
                ProductAttributeEntity entity = new() {
                    Id = Guid.NewGuid().ToString(),
                    Name = productAttribute.Name,
                    CategoryId = category.Id
                };
                productAttributeRepo.Create(entity);
                productAttributeRepo.Save();
                var productAttributeResponse = mapper.Map<ProductAttributeResponse>(entity);
                productAttrResponses.Add(productAttributeResponse);
            }
            var response = mapper.Map<CategoryResponse>(category);
            response.productAttributes = productAttrResponses;
            GenerateImageUrl(response);
            return response;
        }

        public CategoryResponse UpdateCategory(UpdateCategoryRequest request, IFormFile image, List<UpdateProductAttributeRequest> productAttributeRequests) {
            string id = request.Id!;
            var category = categoryRepo.FindById(id, c => c.ProductAttributeEntities);
            if (category == null)
                throw new NotFoundException($"Khong tim thay category voi id = {id}"); 
            category.Name = request.Name;
            category.Description = request.Description;
            if (image != null) {
                storageService.Delete(category.Image!);
                category.Image = storageService.Save(image);
            }
            categoryRepo.Update(category);
            categoryRepo.Save();

            if (productAttributeRequests!= null && productAttributeRequests.Count != 0) {
                foreach (var productAttributeRequest in productAttributeRequests) {
                    var productAttributeEntity = productAttributeRepo.FindById(productAttributeRequest.Id!);
                    if (productAttributeEntity == null)
                        throw new NotFoundException($"Khong tim thay thuoc tinh san pham voi id = {productAttributeRequest.Id}");
                    productAttributeEntity.Name = productAttributeRequest.Name;
                    productAttributeRepo.Update(productAttributeEntity);
                    productAttributeRepo.Save();
                }
            }

            CategoryResponse response = mapper.Map<CategoryResponse>(category);
            var productAttributes = category.ProductAttributeEntities;
            List<ProductAttributeResponse> productAttributeResponses = new List<ProductAttributeResponse>();
            foreach (var productAttribute in productAttributes) {
                var productAttributeResponse = mapper.Map<ProductAttributeResponse>(productAttribute);
                productAttributeResponses.Add(productAttributeResponse);
            }
            response.productAttributes = productAttributeResponses;
            GenerateImageUrl(response);
            return response;
        }

        public string DeleteCategory(string id) {
            var category = categoryRepo.FindById(id);
            if (category == null)
                throw new NotFoundException($"Khong tim thay category voi id = {id}");
            storageService.Delete(category.Image!);
            categoryRepo.Delete(category);
            categoryRepo.Save();
            return "Xoa du lieu thanh cong";
        }

        private void GenerateImageUrl(CategoryResponse response) {
            string imageUrl = storageService.GetImageUrl(response.Image!);
            response.Image = imageUrl;
        }
    }
}