using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text.RegularExpressions;
using AutoMapper;
using BCrypt.Net;
using cnpmm_be.DTO.ExceptionModel;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Entities;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly IUserRepo userRepo;

        private readonly IEmailSenderService emailSenderService;

        private readonly IConfiguration configuration;

        private readonly IMapper mapper = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();


        public UserService(IUserRepo userRepo, IConfiguration configuration, IEmailSenderService emailSenderService) {
            this.userRepo = userRepo;
            this.configuration = configuration;
            this.emailSenderService = emailSenderService;
        }

        public List<UserResponse> GetAllUser() {
            var userList = userRepo.FindAll();
            List<UserResponse> responses = new List<UserResponse>();
            foreach (var user in userList) {
                var response = mapper.Map<UserResponse>(user);
                responses.Add(response);
            }
            return responses;
        }

        public UserResponse UpdateUserStatus(UpdateUserRequest request) {
            string id = request.Id!;
            var user = userRepo.FindById(id);
            if (user == null)
                throw new NotFoundException($"Khong tim thay nguoi dung voi id = {id}");
            user.Status = request.Status;
            user.LastUpdatedAt = new DateTime();
            userRepo.Update(user);
            userRepo.Save();
            return mapper.Map<UserResponse>(user);
        }

        public UserResponse GetUserProfile(string id) {
            var user = userRepo.FindById(id);
            if (user == null)
                throw new NotFoundException($"Khong tim thay nguoi dung voi id = {id}");
            return mapper.Map<UserResponse>(user);
        }

        public AuthResponse Login(LoginRequest request) {
            var user = userRepo.FindByCondition(u => u.Email == request.Email).FirstOrDefault();

            if (user == null)
                throw new UnauthorizedException("Email khong ton tai");

            if (!VerifyPassword(request.Password!, user.Password!))
                throw new UnauthorizedException("Mat khau khong dung");
            
            AuthResponse response = new AuthResponse {
                AccessToken = GenerateToken(user),
                User = mapper.Map<UserResponse>(user)
            };
            return response;
        }

        public UserResponse RegisterUser(RegisterRequest request) {
            if (request.Email == null)
                throw new BadRequestException("Email khong duoc bo trong");

            if (!IsValidEmail(request.Email))
                throw new BadRequestException("Email khong hop le");

            var user = userRepo.FindByCondition(u => u.Email == request.Email).FirstOrDefault();
            if (user != null)
                throw new BadRequestException("Email da ton tai");
            
            if (request.Password == null)
                throw new BadRequestException("Password khong duoc bo trong");
            
            if (request.FullName == null)
                throw new BadRequestException("FullName khong duoc bo trong");

            request.Password = HashPassword(request.Password);
            UserEntity userEntity = mapper.Map<UserEntity>(request);
            userEntity.Id = Guid.NewGuid().ToString();
            userEntity.Role = "USER";
            userEntity.Status = true;
            userRepo.Create(userEntity);
            userRepo.Save();

            return mapper.Map<UserResponse>(userEntity);
        }

        private string GenerateToken(UserEntity user)
        {
            var appSetting = configuration.GetSection("AppSettings");
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var secretKeyBytes = Encoding.UTF8.GetBytes(appSetting["SecretKey"]!);

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new [] {
                    new Claim("uid", user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role!),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKeyBytes), 
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescription);
            return jwtTokenHandler.WriteToken(token);
        }

        public string ChangePassword(ChangePasswordRequest request, string userId) {
            var user = userRepo.FindById(userId);
            string password = request.Password!;
            string newPassword = request.NewPassword!;

            if (!VerifyPassword(password, user.Password!))
                throw new UnauthorizedException("Mat khau khong dung");

            if (password == newPassword)
                throw new BadRequestException("Mat khau moi trung voi mat khau hien tai");

            string hashedPassword = HashPassword(newPassword);
            user.Password = hashedPassword;
            user.LastUpdatedAt = DateTime.Now;
            userRepo.Update(user);
            userRepo.Save();
            return "Doi mat khau thanh cong";
        }

        public string ResetPassword(string email) {
            if (!IsValidEmail(email))
            {
                throw new BadRequestException("Email khong hop le");
            }

            var user = userRepo!.FindByCondition(u => u.Email == email).FirstOrDefault();
            if (user == null)
                throw new NotFoundException($"Khong ton tai nguoi dung voi email la {email}");

    
            string newPassword = GenerateRandomPassword();
            string hashedPassword = HashPassword(newPassword);
            
            user.Password = hashedPassword;
            user.LastUpdatedAt = DateTime.Now;
            userRepo.Update(user);
            userRepo.Save();

            emailSenderService.SendPasswordViaEmail(email, newPassword, user.FullName!);

            return "Mat khau moi da duoc gui qua email";
        }

        public UserResponse UpdateProfile(string userId, UpdateProfileRequest request) {
            var user = userRepo.FindById(userId);
            if (user == null)
                throw new NotFoundException($"Khong tim thay user voi id = {userId}");
            if (request.FullName != null)
                user.FullName = request.FullName;

            if (request.Email != null)
                user.Email = request.Email;
            user.LastUpdatedAt = new DateTime();
            userRepo.Update(user);
            userRepo.Save();
            return mapper.Map<UserResponse>(user);  
        }

        public string HashPassword(string password)
        {
            return BCrypt.Net.BCrypt.EnhancedHashPassword(password, HashType.SHA256);
        }

        public bool VerifyPassword(string password, string hashedPassword)
        {
            return BCrypt.Net.BCrypt.EnhancedVerify(password, hashedPassword, HashType.SHA256);
        }

        private bool IsValidEmail(string email)
        {
            string pattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(email);
        }

        private string GenerateRandomPassword()
        {
            const string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            StringBuilder passwordBuilder = new StringBuilder();

            Random random = new Random();
            for (int i = 0; i < 8; i++)
            {
                int index = random.Next(characters.Length);
                passwordBuilder.Append(characters[index]);
            }

            string newPassword = passwordBuilder.ToString();
            return newPassword;
        }
    }
}