using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;

namespace cnpmm_be.Services.Impl
{
    public class EmailSenderService : IEmailSenderService
    {
        private readonly IConfiguration configuration;

        public EmailSenderService(IConfiguration configuration) {
            this.configuration = configuration;
        }
        
        public void SendPasswordViaEmail(string email, string newPassword, string recipientName) {
            var appSetting = configuration.GetSection("AppSettings");
            string emailSubject = "Về việc khôi phục mật khẩu";
            string emailBody = $"<p style='font-size: 16px'>Mật khẩu mới của bạn là: </p><h3>{newPassword}</h3>";

            string smtpServer = appSetting["SmtpServer"]!;
            int smtpPort = 587;
            string smtpUsername = appSetting["SmtpUsername"]!;
            string smtpPassword = appSetting["SmtpPassword"]!;

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Nhóm STT 4", smtpPassword));
            message.To.Add(new MailboxAddress(recipientName, email));
            message.Subject = emailSubject;

            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = emailBody;

            message.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient()) {
                client.Connect(smtpServer, smtpPort, SecureSocketOptions.StartTls);
                client.Authenticate(smtpUsername, smtpPassword);
                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}