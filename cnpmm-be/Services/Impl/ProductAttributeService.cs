using AutoMapper;
using cnpmm_be.DTO.Response;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class ProductAttributeService : IProductAttributeService
    {
        private readonly IProductAttributeRepo productAttributeRepo;

        private readonly IMapper mapper = new MapperConfiguration(cfg => {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();

        public ProductAttributeService(IProductAttributeRepo productAttributeRepo) {
            this.productAttributeRepo = productAttributeRepo;
        }

        public List<ProductAttributeResponse> getAllProductAttributeByCategory(string categoryId) {
            var productAttributeList = productAttributeRepo.FindByCondition(p => p.CategoryId == categoryId);
            List<ProductAttributeResponse> productAttributeResponses = new List<ProductAttributeResponse>();
            foreach (var productAttribute in productAttributeList) {
                ProductAttributeResponse productAttributeResponse = mapper.Map<ProductAttributeResponse>(productAttribute);
                productAttributeResponses.Add(productAttributeResponse);
            }
            return productAttributeResponses;
        }
    }
}