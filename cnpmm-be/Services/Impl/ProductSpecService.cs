using AutoMapper;
using cnpmm_be.DTO.ExceptionModel;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class ProductSpecService : IProductSpecService
    {
        private readonly IProductSpecRepo productSpecRepo;

        private readonly IMapper mapper = new MapperConfiguration(cfg => {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();

        public ProductSpecService(IProductSpecRepo productSpecRepo) {
            this.productSpecRepo = productSpecRepo;
        }

        public void UpdateProductSpec(UpdateProductSpecRequest request) {
            string id = request.Id;
            var productSpec = productSpecRepo.FindById(id);
            if (productSpec == null)
                throw new NotFoundException($"Khong tim thay product spec voi id = {id}");
            productSpec.Value = request.Value;
            productSpec.ProductAttributeId = request.ProductAttributeId;
            productSpec.LastUpdatedAt = new DateTime();
            productSpecRepo.Update(productSpec);
            productSpecRepo.Save();
        }
    }
}