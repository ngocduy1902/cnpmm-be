using AutoMapper;
using cnpmm_be.Base;
using cnpmm_be.DTO.ExceptionModel;
using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;
using cnpmm_be.Entities;
using cnpmm_be.Repositories;
using cnpmm_be.Services.Impl.ObjectMapping;

namespace cnpmm_be.Services.Impl
{
    public class BrandService : IBrandService
    {
        private readonly IBrandRepo brandRepo;

        private readonly IGoogleCloudStorageService storageService;

        private readonly IMapper mapper = new MapperConfiguration(cfg => {
            cfg.AddProfile(new MappingProfile());
        }).CreateMapper();

        public BrandService(IBrandRepo brandRepo, IGoogleCloudStorageService storageService) {
            this.brandRepo = brandRepo;
            this.storageService = storageService;
        }

        public BrandResponse Create(CreateBrandRequest request) {
            BrandEntity brand = mapper.Map<BrandEntity>(request);
            brand.Id = Guid.NewGuid().ToString();
            brand.Image = storageService.Save(request.Image!);
            brandRepo.Create(brand);
            brandRepo.Save();
            BrandResponse response = mapper.Map<BrandResponse>(brand);
            response.Image = storageService.GetImageUrl(response.Image!);
            return response;
        }

        public List<BrandResponse> getAllBrands() {
            List<BrandEntity> brandList = brandRepo.FindAll();
            List<BrandResponse> responses = new List<BrandResponse>();
            foreach (BrandEntity entity in brandList) {
                BrandResponse response = mapper.Map<BrandResponse>(entity);
                response.Image = storageService.GetImageUrl(response.Image!);
                responses.Add(response);
            }
            return responses;
        }

        public BrandResponse getBrandById(string id) {
            BrandEntity brand = brandRepo.FindById(id);
            if (brand == null) {
                throw new NotFoundException($"Khong ton tai brand voi id = {id}");
            }

            BrandResponse response = mapper.Map<BrandResponse>(brand);
            response.Image = storageService.GetImageUrl(response.Image!);
                
            return response;
        }

        public BrandResponse UpdateBrand(UpdateBrandRequest request) {
            string id = request.Id!;
            var brand = brandRepo.FindById(id);
            if (brand == null)
                throw new NotFoundException($"Khong tim thay brand voi id = {id}");
            brand.Name = request.Name;
            if (request.Image != null) {
                storageService.Delete(brand.Image!);
                brand.Image = storageService.Save(request.Image);
            }
            brandRepo.Update(brand);
            brandRepo.Save();
            BrandResponse response = mapper.Map<BrandResponse>(brand);
            response.Image = storageService.GetImageUrl(response.Image!);
            return response;
        }
        
        public string DeleteBrand(string id) {
            var brand = brandRepo.FindById(id);
            if (brand == null)
                throw new NotFoundException($"Khong tim thay brand voi id = {id}");
            brandRepo.Delete(brand);
            brandRepo.Save();
            return "Xoa du lieu thanh cong";
        }
    }
}