using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface IProductAttributeService
    {
        List<ProductAttributeResponse> getAllProductAttributeByCategory(string categoryId);
    }
}