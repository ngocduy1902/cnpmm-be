using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface IBrandService
    {
        BrandResponse Create(CreateBrandRequest request);
        List<BrandResponse> getAllBrands();
        BrandResponse getBrandById(string id);
        BrandResponse UpdateBrand(UpdateBrandRequest request);
        string DeleteBrand(string id);
    }
}