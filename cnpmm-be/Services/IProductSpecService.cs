using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface IProductSpecService
    {
        void UpdateProductSpec(UpdateProductSpecRequest request);
    }
}