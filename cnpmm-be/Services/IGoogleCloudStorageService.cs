namespace cnpmm_be.Services
{
    public interface IGoogleCloudStorageService
    {
        string Save(IFormFile file);
        void Delete(string fileName);
        string GetImageUrl(string fileName);
    }
}