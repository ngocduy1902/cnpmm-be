using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface ICategoryService
    {
        List<CategoryResponse> GetAllCategory();
        CategoryResponse GetCategoryById(string id);
        CategoryResponse CreateCategory(CreateCategoryRequest request, IFormFile image, List<CreateProductAttributeRequest> productAttributes);
        CategoryResponse UpdateCategory(UpdateCategoryRequest request, IFormFile image, List<UpdateProductAttributeRequest> productAttributeRequests);
        string DeleteCategory(string id);
    }
}