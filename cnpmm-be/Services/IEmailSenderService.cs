namespace cnpmm_be.Services
{
    public interface IEmailSenderService
    {
        void SendPasswordViaEmail(string email, string newPassword, string recipientName);
    }
}