using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface IUserService
    {
        AuthResponse Login(LoginRequest request);
        UserResponse RegisterUser(RegisterRequest request);
        UserResponse GetUserProfile(string id);
        string ChangePassword(ChangePasswordRequest request, string userId);
        string ResetPassword(string email);
        UserResponse UpdateProfile(string userId, UpdateProfileRequest request);
        List<UserResponse> GetAllUser();
        UserResponse UpdateUserStatus(UpdateUserRequest request);
    }
}