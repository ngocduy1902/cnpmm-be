using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface IProductService
    {
        ProductResponse CreateProduct(CreateProductRequest request, List<IFormFile> images, List<CreateProducSpecRequest> productSpecs);
        List<ProductResponse> getAllProduct();
        PaginationResponse getAllProductPage(int CurrentPage, int PageSize);
        PaginationResponse getAllProductByBrandName(string brandName, int currentPage, int pageSize);
        PaginationResponse SearchFilterProduct(Dictionary<string, string> queries);
        ProductResponse GetProductDetail(string id);
        PaginationResponse getAllProductByCategoryName(string categoryName, int currentPage, int pageSize);
        string DeleteProduct(string id);
        ProductResponse UpdateProduct(UpdateProductRequest request, List<IFormFile>? images,
            List<UpdateProductSpecRequest>? productSpecs);
        List<string> GetProductColorByCategory(string categoryId);
    }
}