using cnpmm_be.DTO.Request;
using cnpmm_be.DTO.Response;

namespace cnpmm_be.Services
{
    public interface IOrderService
    {
        OrderResponse CreateOrder(string userId, CreateOrderRequest request);
        List<OrderResponse> GetOrderByUser(string userId);
        OrderDetailResponse GetOrderDetail(string orderId);
        string CancelOrder(UpdateOrderStatusRequest request);
        List<OrderDetailResponse> GetAllOrder();
        OrderDetailResponse UpdateOrderStatus(UpdateOrderStatusRequest request);
        OrderDetailResponse UpdateOrderIsPaid(string id);
    }
}