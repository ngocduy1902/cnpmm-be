using cnpmm_be.DTO.ExceptionModel;
using Microsoft.AspNetCore.Mvc.Filters;

namespace cnpmm_be.Middlewares
{
    public class UserAuthorizationFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context) { 
            if (!context.HttpContext.User.IsInRole("USER"))
            {
                throw new ForbiddenException("Khong co quyen truy cap user");
            }
            return;
        }
    }
}