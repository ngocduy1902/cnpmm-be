using System.Text.Json;
using cnpmm_be.Base;
using cnpmm_be.DTO.ExceptionModel;

namespace cnpmm_be.Middlewares
{
    public class ExceptionHandling
    {
        private readonly RequestDelegate next;

        public ExceptionHandling(RequestDelegate next) {
            this.next = next;
        }

        public async Task Invoke(HttpContext httpContext) {
            try {
                await next(httpContext);
            }
            catch (Exception ex) {
                await HandleException(httpContext, ex);
            }
        }

        private async Task HandleException(HttpContext context, Exception ex) {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = ex switch {
                BadRequestException => StatusCodes.Status400BadRequest,
                NotFoundException => StatusCodes.Status404NotFound,
                UnauthorizedException => StatusCodes.Status401Unauthorized,
                ForbiddenException => StatusCodes.Status403Forbidden,
                ArgumentNullException => StatusCodes.Status400BadRequest,
                _ => StatusCodes.Status500InternalServerError
            };
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            string result = JsonSerializer.Serialize(new ExceptionResponse {
                Message = ex.Message
            }, options);

            await context.Response.WriteAsync(result);
        }
    }
}