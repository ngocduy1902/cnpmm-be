using cnpmm_be.DTO.ExceptionModel;
using Microsoft.AspNetCore.Mvc.Filters;

namespace cnpmm_be.Middlewares
{
    public class AdminAuthorizationFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context) { 
            if (!context.HttpContext.User.IsInRole("ADMIN"))
            {
                throw new ForbiddenException("Khong co quyen truy cap admin");
            }
            return;
        }
    }
}