using cnpmm_be.DTO.ExceptionModel;
using Microsoft.AspNetCore.Mvc.Filters;

namespace cnpmm_be.Middlewares
{
    public class AuthenticationFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!context.HttpContext.User.Identity!.IsAuthenticated)
            {
                throw new UnauthorizedException("Access token khong hop le");
            }
            return;
        }
    }
}