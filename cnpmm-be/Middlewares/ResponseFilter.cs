using cnpmm_be.Base;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace cnpmm_be.Middlewares
{
    public class ResponseFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Result is ObjectResult objectResult) {
                if (context.HttpContext.Response.StatusCode >= 200 && context.HttpContext.Response.StatusCode < 300) {
                    if (objectResult.Value is string message) {
                        objectResult.Value = new SuccessResponse {
                            Message = message
                        };
                    }
                    else {
                        objectResult.Value = new SuccessResponse {
                            Data = objectResult.Value
                        };
                    }
                    
                }
                else {
                    objectResult.Value = new ExceptionResponse {
                        Message = objectResult.Value?.ToString()
                    };
                }
            }
        }
    }
}