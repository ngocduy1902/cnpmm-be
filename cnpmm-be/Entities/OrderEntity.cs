﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class OrderEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Status { get; set; }

    public double? Total { get; set; }

    public string? UserId { get; set; }

    public string? ReceiverPhone {get; set;}

    public string? ReceiverName {get; set;}

    public string? Address {get; set;}

    public bool? IsPaid {get; set;}
    
    public string? Payment {get; set;}

    public virtual ICollection<OrderItemEntity> OrderItemEntities { get; set; } = new List<OrderItemEntity>();

    public virtual UserEntity? User { get; set; }
}
