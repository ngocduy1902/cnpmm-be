﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class ProductSpecEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Value { get; set; }

    public string? ProductId { get; set; }

    public string? ProductAttributeId { get; set; }

    public virtual ProductEntity? Product { get; set; }

    public virtual ProductAttributeEntity? ProductAttribute { get; set; }
}
