namespace cnpmm_be.Entities
{
    public partial class ProductImageEntity : IEntity
    {
        public string Id { get; set; } = null!;

        public DateTime? CreatedAt { get; set; }

        public DateTime? DeletedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public string? Name {get; set;}
        public string? ProductId { get; set; }
        public virtual ProductEntity? Product {get; set;}
    }
}