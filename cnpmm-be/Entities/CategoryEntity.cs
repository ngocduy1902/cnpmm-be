﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class CategoryEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public string? Image { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Name { get; set; }

    public string? Description {get; set;}

    public virtual ICollection<ProductAttributeEntity> ProductAttributeEntities { get; set; } = new List<ProductAttributeEntity>();

    public virtual ICollection<ProductEntity> ProductEntities { get; set; } = new List<ProductEntity>();
}
