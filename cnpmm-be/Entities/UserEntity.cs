﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class UserEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public string? Email { get; set; }

    public string? FullName { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Password { get; set; }

    public string? Role { get; set; }

    public bool? Status { get; set; }

    public virtual ICollection<OrderEntity> OrderEntities { get; set; } = new List<OrderEntity>();
}
