namespace cnpmm_be.Entities
{
    public interface IEntity
    {
        public string Id { get; set; }
        public DateTime? DeletedAt { get; set; }
        public DateTime? CreatedAt { get; set; }

        public DateTime? LastUpdatedAt { get; set; }
    }
}