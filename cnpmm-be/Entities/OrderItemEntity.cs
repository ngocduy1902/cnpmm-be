﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class OrderItemEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? ProductName { get; set; }

    public double? ProductPrice { get; set; }

    public int? Quantity { get; set; }

    public string? OrderId { get; set; }

    public string? ProductId { get; set; }

    public virtual OrderEntity? Order { get; set; }

    public virtual ProductEntity? Product { get; set; }
}
