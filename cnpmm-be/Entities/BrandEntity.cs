﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class BrandEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public string? Image { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Name { get; set; }

    public virtual ICollection<ProductEntity> ProductEntities { get; set; } = new List<ProductEntity>();
}
