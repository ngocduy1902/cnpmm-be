﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class ProductAttributeEntity : IEntity
{
    public string Id { get; set; } = null!;

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Name { get; set; }

    public string? CategoryId { get; set; }

    public virtual CategoryEntity? Category { get; set; }

    public virtual ICollection<ProductSpecEntity> ProductSpecEntities { get; set; } = new List<ProductSpecEntity>();
}
