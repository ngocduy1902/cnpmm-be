﻿using System;
using System.Collections.Generic;

namespace cnpmm_be.Entities;

public partial class ProductEntity : IEntity
{
    public string Id { get; set; } = null!;

    public string? Color { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? DeletedAt { get; set; }

    public string? Description { get; set; }

    public string? Guarantee { get; set; }

    public DateTime? LastUpdatedAt { get; set; }

    public string? Name { get; set; }

    public bool? OnSale { get; set; }

    public double? Price { get; set; }

    public int? Quantity { get; set; }

    public double? SalePrice { get; set; }

    public int? Sold { get; set; }

    public bool? Status { get; set; }

    public string? BrandId { get; set; }

    public string? CategoryId { get; set; }

    public virtual BrandEntity? Brand { get; set; }

    public virtual CategoryEntity? Category { get; set; }

    public virtual ICollection<OrderItemEntity> OrderItemEntities { get; set; } = new List<OrderItemEntity>();

    public virtual ICollection<ProductSpecEntity> ProductSpecEntities { get; set; } = new List<ProductSpecEntity>();

    public virtual ICollection<ProductImageEntity> ProductImageEntities {get; set; } = new List<ProductImageEntity>();
}
